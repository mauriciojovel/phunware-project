package com.mad.phunware.venue.utils;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.TrustManager;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.conn.ssl.X509HostnameVerifier;
import android.annotation.SuppressLint;
import android.util.Log;

/**
 * Accept all SSL Connection.
 * Fix problem with untrusted ssl connection in android 2.3 or lower.
 * @author Mauricio Jovel.
 *
 */
public class SSLConection {
  
  // Trust maanger
	private static TrustManager[] trustManagers;

	public static class FakeX509TrustManager implements
			javax.net.ssl.X509TrustManager {
		private static final X509Certificate[] ACCEPTED_ISSUERS = 
													new X509Certificate[] {};

		public void checkClientTrusted(X509Certificate[] arg0, String arg1)
				throws CertificateException {
		}

		public void checkServerTrusted(X509Certificate[] arg0, String arg1)
				throws CertificateException {
		}

		public X509Certificate[] getAcceptedIssuers() {
			return (ACCEPTED_ISSUERS);
		}
	}

	@SuppressLint("TrulyRandom")
	public static void allowAllSSL() {
		HostnameVerifier hostnameVerifier = 
		org.apache.http.conn.ssl.SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER;

		SchemeRegistry registry = new SchemeRegistry();
		SSLSocketFactory socketFactory = SSLSocketFactory.getSocketFactory();
		socketFactory
				.setHostnameVerifier((X509HostnameVerifier) hostnameVerifier);
		registry.register(new Scheme("https", socketFactory, 443));

		javax.net.ssl.HttpsURLConnection
				.setDefaultHostnameVerifier(hostnameVerifier);

		javax.net.ssl.SSLContext context;

		if (trustManagers == null) {
			trustManagers = new TrustManager[] { new FakeX509TrustManager() };
		}

		try {
			context = javax.net.ssl.SSLContext.getInstance("TLS");
			context.init(null, trustManagers, new SecureRandom());
			javax.net.ssl.HttpsURLConnection.setDefaultSSLSocketFactory(context
					.getSocketFactory());
		} catch (NoSuchAlgorithmException e) {
			Log.e("allowAllSSL", e.toString());
		} catch (KeyManagementException e) {
			Log.e("allowAllSSL", e.toString());
		}
	}
}
