package com.mad.phunware.venue.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Checkable;
import android.widget.LinearLayout;

/**
 * Convert a LinearLayout checkable for backward compatibility 3 
 * , based on the
 * examples found in: 
 * http://tokudu.com/post/50023900640/android-checkable-linear-layout
 * 
 * @author Mauricio Jovel
 * 
 */
public class CheckableLinearLayout extends LinearLayout implements Checkable {

  // Indicate is layout is checked
  private boolean mChecked = false;

  /**
   * Default constructor.
   * 
   * @param context
   *          Activity called.
   * @param attrs
   *          Attributes.
   */
  public CheckableLinearLayout(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  @Override
  public boolean isChecked() {
    return mChecked;
  }

  @Override
  public void setChecked(boolean checked) {
    this.mChecked = checked;
    super.setSelected(checked);

  }

  @Override
  public void toggle() {
    setChecked(!mChecked);
  }

}
