package com.mad.phunware.venue;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;

import com.mad.phunware.venue.R;
import com.mad.phunware.venue.fragments.HelpFragment;
import com.mad.phunware.venue.fragments.VenueItemFragment;
import com.mad.phunware.venue.fragments.ListFragment.OnVenueSelected;

/**
 * The principal Activity.
 * 
 * @author Mauricio Jovel.
 * 
 */
public class MainActivity extends BasicActivity implements OnVenueSelected {

  // Flag that indicates If two panes is available
  // , the file you change the value is bool.xml
  private boolean mIsTwoPanels;

  // Current position selected.
  private int mCurrentPos;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    mIsTwoPanels = getResources().getBoolean(R.bool.is_two_panels);
    if (savedInstanceState != null) {
      mCurrentPos = savedInstanceState.getInt("mCurrentPos", -1);
    } else {
      mCurrentPos = -1;
    }
    showHelp();
  }

  @Override
  public void onVenueSelected(int pos) {
    Bundle b = new Bundle();
    VenueItemFragment venueItemFragment = null;
    Intent intent = null;
    b.putInt(VenueItemFragment.EXTRA_SELECTED_ITEM, pos);
    if (mIsTwoPanels) {
      // Use fragments if two panel is available.
      venueItemFragment = new VenueItemFragment();
      venueItemFragment.setArguments(b);
      getSupportFragmentManager().beginTransaction()
          .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
          .replace(R.id.container, venueItemFragment)
          .commit();
    } else {
      // Use intent if two panel is not available.
      intent = new Intent(this, ItemActivity.class);
      intent.putExtras(b);
      startActivity(intent);
    }
    mCurrentPos = pos;
  }

  @Override
  protected void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);
    outState.putInt("mCurrentPos", mCurrentPos);
  }

  /**
   * This method decide if the help panel is show.
   */
  private void showHelp() {
    if (mIsTwoPanels && mCurrentPos < 0) {
      // If is two panels and empty selection, show the help fragment
      getSupportFragmentManager().beginTransaction()
          .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
          .replace(R.id.container, new HelpFragment()).commit();
    }
  }

}
