package com.mad.phunware.venue;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;

/**
 * Basic activity represents the most commons method in all Activity.
 * @author Mauricio Jovel
 *
 */
public class BasicActivity extends ActionBarActivity {
	  /**
     * send broadcast message.
     * @param notification notification 
     * @param ext data.
     */
    protected void publishResults(String notification, Intent ext) {
        Intent intent = new Intent(notification);
        if(ext != null) {
            intent.putExtras(ext);
        }
        sendBroadcast(intent);
    }
}
