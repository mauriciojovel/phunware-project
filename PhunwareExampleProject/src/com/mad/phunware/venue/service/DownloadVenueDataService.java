package com.mad.phunware.venue.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.IntentService;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;

import com.mad.phunware.venue.json.ScheduleItem;
import com.mad.phunware.venue.json.Venue;
import com.mad.phunware.venue.utils.SSLConection;

/**
 * Service to downloaded data from the server.
 * 
 * @author Mauricio Jovel.
 * 
 */
public class DownloadVenueDataService extends IntentService {

  /**
   * Should be used to {@link IntentFilter} notification.
   */
  public static final String NOTIFICATION = "notification.dowload.data";

  /**
   * The downloaded data.
   */
  // Normally this data is save in sqlite but in this case
  // save in memory.
  public static List<Venue> data = null;

  /**
   * Default Constructor.
   */
  public DownloadVenueDataService() {
    super("DownloadVenueDataService");
  }

  @Override
  protected void onHandleIntent(Intent intent) {
    readJson();
  }

  /**
   * Make the request to amazon server and downloaded the data.
   * This method fill the field
   * {@link DownloadVenueDataService.DATA}
   */
  public void readJson() {
    InputStream inputStream = null;
    String result = "";
    String urlSelect = 
        "https://s3.amazonaws.com/jon-hancock-phunware/nflapi-static.json";
    HttpClient httpClient = null;

    HttpGet httpPost = null;
    HttpResponse httpResponse = null;
    HttpEntity httpEntity = null;

    BufferedReader bReader = null;
    StringBuilder sBuilder = null;

    JSONArray venues = null;
    JSONArray schedules = null;
    JSONObject venue = null;
    JSONObject schedule = null;
    Venue venuData = null;

    int size = -1;

    try {
      // Set up HTTP post

      httpClient = new DefaultHttpClient();
      SSLConection.allowAllSSL();

      httpPost = new HttpGet(urlSelect);
      httpResponse = httpClient.execute(httpPost);
      httpEntity = httpResponse.getEntity();
      
      // Read content & Log
      inputStream = httpEntity.getContent();

      bReader = new BufferedReader(new InputStreamReader(inputStream
                                                          , "iso-8859-1"), 8);
      sBuilder = new StringBuilder();

      String line = null;
      while ((line = bReader.readLine()) != null) {
        sBuilder.append(line).append("\n");
      }

      inputStream.close();
      result = sBuilder.toString();

      venues = new JSONArray(result);
      size = venues.length();

      if (size > 0) {
        data = new ArrayList<Venue>();
      } else {
        data = null;
      }
      // I usually save the data when executing this code (getContentResolver()). 
      // The document asked " Do not store this file locally in the app."
      // TODO ask if like save the data in a sqlite database.
      for (int i = 0; i < size; i++) {
        venue = venues.getJSONObject(i);
        schedules = venue.getJSONArray("schedule");
        venuData = new Venue();
        venuData.setZip(venue.getString("zip"));
        venuData.setPhone(venue.getString("phone"));
        venuData.setTicketLink(venue.getString("ticket_link"));
        venuData.setState(venue.getString("state"));
        venuData.setPcode(Integer.parseInt(venue.getString("pcode")));
        venuData.setCity(venue.getString("city"));
        venuData.setId(Long.parseLong(venue.getString("id")));
        venuData.setTollFreePhone(venue.getString("tollfreephone"));
        venuData.setAddress(venue.getString("address"));
        venuData.setImageUrl(venue.getString("image_url"));
        venuData.setDescription(venue.getString("description"));
        venuData.setName(venue.getString("name"));
        venuData.setLongitude(Double.parseDouble(venue.getString("longitude")));
        venuData.setLatitude(Double.parseDouble(venue.getString("latitude")));
        venuData.setSchedule(new ArrayList<ScheduleItem>());
        for (int j = 0; j < schedules.length(); j++) {
          schedule = schedules.getJSONObject(j);
          venuData.getSchedule().add(
              new ScheduleItem(schedule.getString("start_date")
                  , schedule.getString("end_date")));
        }

        data.add(venuData);
      }
    } catch (UnsupportedEncodingException e1) {
      Log.e("UnsupportedEncodingException", e1.toString(), e1);
    } catch (ClientProtocolException e2) {
      Log.e("ClientProtocolException", e2.toString(), e2);
    } catch (IllegalStateException e3) {
      Log.e("IllegalStateException", e3.toString(), e3);
    } catch (IOException e4) {
      Log.e("IOException", e4.toString(), e4);
    } catch (JSONException e) {
      Log.e("IOException", e.toString(), e);
    } catch (ParseException e) {
      Log.e("ParsionException", e.getMessage(), e);
    }

    // Tell to world the data is downloaded.
    publishResults(NOTIFICATION, null);
  }

  /**
   * send broadcast message
   * 
   * @param notification
   *          notification
   * @param ext
   *          data.
   */
  protected void publishResults(String notification, Intent ext) {
    Intent intent = new Intent(notification);
    if (ext != null) {
      intent.putExtras(ext);
    }
    sendBroadcast(intent);
  }
}
