package com.mad.phunware.venue.fragments;

import com.mad.phunware.venue.R;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Simple Fragment to show a help.
 * @author Mauricio Jovel
 *
 */
public class HelpFragment extends Fragment {
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.help_layout, container, false);
	}
}
