package com.mad.phunware.venue.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.ShareActionProvider;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.mad.phunware.venue.R;
import com.mad.phunware.venue.json.ScheduleItem;
import com.mad.phunware.venue.json.Venue;
import com.mad.phunware.venue.service.DownloadVenueDataService;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

/**
 * Responsible to show a one Venue item.
 * 
 * @author Mauricio Jovel.
 * 
 */
public class VenueItemFragment extends Fragment {
	/**
	 * Should be used to indicate a fragment the selected item.
	 */
	public static final String EXTRA_SELECTED_ITEM = "selected_item";

	// Name of Venue
	private TextView mNameTV;

	// Address of Venue
	private TextView mAddressTV;

	// Ubication of Venue.
	private TextView mUbicationTV;

	// Image to show.
	private ImageView mImageView;
	
	// The Schedule List.
	private TextView mScheduleListTextView;

	// Venue selected
	private Venue mVenue;

	// Progress Bar to show when image is loading.
	private ProgressBar mProgress;

	// References shared provider menu.
	private ShareActionProvider mShareProvider;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View root = inflater.inflate(R.layout.fragment_item, container, false);
		mNameTV = (TextView) root.findViewById(R.id.nameTextView);
		mAddressTV = (TextView) root.findViewById(R.id.addressTextView);
		mUbicationTV = (TextView) root.findViewById(R.id.ubicationTextView);
		mImageView = (ImageView) root.findViewById(R.id.venueImageView);
		mProgress = (ProgressBar) root.findViewById(android.R.id.progress);
		mScheduleListTextView = 
		                  (TextView) root.findViewById(R.id.scheduleListTextView);
		return root;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		StringBuilder buffer = null;
		String lineSeparator = System.getProperty("line.separator");
		String space = " ";
		java.text.DateFormat longDateFormat = 
        DateFormat.getMediumDateFormat(getActivity());
    java.text.DateFormat hourFormat = DateFormat.getTimeFormat(getActivity());
		mVenue = getVenue();
		if (!TextUtils.isEmpty(mVenue.getImageUrl())) {
			// Try to downloaded image with picasso
			Picasso.with(getActivity()).load(mVenue.getImageUrl())
					.placeholder(R.drawable.ic_launcher)
					.error(R.drawable.ic_launcher)
					.into(mImageView, new Callback() {

						@Override
						public void onError() {
							showImage();
							Toast.makeText(getActivity(),
									getString(R.string.error_download),
									Toast.LENGTH_SHORT).show();
						}

						@Override
						public void onSuccess() {
							showImage();
						}

						private void showImage() {
							mProgress.setVisibility(View.GONE);
							mImageView.setVisibility(View.VISIBLE);
						}

					});
		} else {
			// Show the image with text "not image"
			mProgress.setVisibility(View.GONE);
			mImageView.setImageResource(R.drawable.no_image);
			mImageView.setVisibility(View.VISIBLE);
		}

		// Load the text data.
		mNameTV.setText(mVenue.getName());
		mAddressTV.setText(mVenue.getAddress());
		mUbicationTV.setText(mVenue.getCity() + ", " + mVenue.getState() + " "
				+ mVenue.getZip());

		// Create the adapter to show schedules.
		if (mVenue.getSchedule().isEmpty()) {
			// If not have schedules show the empty text.
			mScheduleListTextView.setText(getString(R.string.no_schedule));
		} else {
		  buffer = new StringBuilder();
		  for (ScheduleItem schedule : mVenue.getSchedule()) {
        buffer.append(longDateFormat.format(schedule.getStartDate()))
              .append(space)
              .append(hourFormat.format(schedule.getStartDate()))
              .append(space)
              .append(getString(R.string.to))
              .append(space)
              .append(hourFormat.format(schedule.getEndDate()))
              .append(lineSeparator)
              .append(lineSeparator);
      }
		  mScheduleListTextView.setText(buffer.toString());
		}
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		MenuItem sharedMenuItem = null;
		inflater.inflate(R.menu.selected, menu);
		// Get the menu item.
		sharedMenuItem = menu.findItem(R.id.action_share);
		// Get the provider and hold onto it to set/change the share intent.
		mShareProvider = (ShareActionProvider) MenuItemCompat
				.getActionProvider(sharedMenuItem);

		// Set history different from the default before getting the action
		mShareProvider.setShareHistoryFileName("custom_share_history.xml");
		
		// Set the intent.
		mShareProvider.setShareIntent(getDefaultIntent());
	}

	/**
	 * @return The selected {@link Venue}
	 */
	private Venue getVenue() {
		return DownloadVenueDataService.data.get(getArguments().getInt(
				EXTRA_SELECTED_ITEM));
	}

	/**
	 * Defines a default share intent to initialize the action provider. This
	 * intent have the {@link Venue} data
	 */
	private Intent getDefaultIntent() {
		Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
		Venue v = getVenue();
		String lineSeparator = System.getProperty("line.separator");
		String space = " ";
		StringBuilder builder = new StringBuilder();
		builder.append(v.getName()).append(lineSeparator)
				.append(v.getAddress()).append(lineSeparator)
				.append(v.getCity()).append(space).append(v.getState())
				.append(space).append(v.getZip()).append(lineSeparator)
				.append(getString(R.string.share_by));
		sharingIntent.setType("text/plain");
		sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,
				v.getName());
		sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT,
				builder.toString());
		return sharingIntent;
	}
}
