package com.mad.phunware.venue.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.mad.phunware.venue.R;
import com.mad.phunware.venue.adapter.VenueListAdapter;
import com.mad.phunware.venue.json.Venue;
import com.mad.phunware.venue.service.DownloadVenueDataService;

/**
 * this class represent a list of venue items.
 * 
 * @author Maurcio Jovel
 * 
 */
// Normally this class implements LoaderCallback but in this case not
// because the data is in memory.
public class ListFragment extends android.support.v4.app.ListFragment {

  // Adapter handle data show.
  private ArrayAdapter<Venue> mAdapter;

  // Parent Activity to handle select item.
  private OnVenueSelected mListener;

  // Indicate if the data should be load.
  private boolean mLoadData;

  // Index of select item.
  private int mIndex;

  // Position of top item.
  private int mTop;

  /**
   * Handle the event select a one item of list.
   * 
   * @author Mauricio Jovel
   * 
   */
  public interface OnVenueSelected {
    /**
     * Called when a item is selected.
     * 
     * @param pos
     *          position of item.
     */
    public void onVenueSelected(int pos);
  }

  // Receiver that handle the downloaded data.
  private BroadcastReceiver mReciver = new BroadcastReceiver() {

    @Override
    public void onReceive(Context context, Intent intent) {
      showItems();
    }

  };

  /**
   * Class to check the network status.
   */
  private BroadcastReceiver mNetworkStatus = new BroadcastReceiver() {
    @Override
    public void onReceive(Context context, Intent intent) {

      boolean status = getConnectivityStatus(getActivity());

      if (status) {
        
      } else {
        Toast.makeText(getActivity(), getString(R.string.offline)
            , Toast.LENGTH_SHORT).show();
      }
    }
  };

  @Override
  public void onAttach(android.app.Activity activity) {
    super.onAttach(activity);
    if (activity instanceof OnVenueSelected) {
      mListener = (OnVenueSelected) activity;

    }
  };

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    if (savedInstanceState != null) {
      mLoadData = savedInstanceState.getBoolean("mLoadData", true);
      mTop = savedInstanceState.getInt("mTop");
      mIndex = savedInstanceState.getInt("mIndex");
    } else {
      mLoadData = true;
    }
  }

  @Override
  public void onActivityCreated(Bundle savedInstanceState) {
    super.onActivityCreated(savedInstanceState);
    if (mLoadData) {
      loadList();
    } else {
      showItems();
    }
    getListView().setChoiceMode(ListView.CHOICE_MODE_SINGLE);
  }

  @Override
  public void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);
    View currentRow = getListView().getChildAt(0);
    mIndex = getListView().getFirstVisiblePosition();
    // If v is null the position is top
    mTop = (currentRow == null) ? 0 : currentRow.getTop();
    outState.putBoolean("mLoadData", mLoadData);
    outState.putInt("mIndex", mIndex);
    outState.putInt("mTop", mTop);
  }

  @Override
  public void onResume() {
    super.onResume();
    getActivity()
        .registerReceiver(mReciver
            , new IntentFilter(DownloadVenueDataService.NOTIFICATION));
    getActivity().registerReceiver(mNetworkStatus
        , new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
    getListView().setSelectionFromTop(mIndex, mTop);
  }

  @Override
  public void onPause() {
    super.onPause();
    getActivity().unregisterReceiver(mReciver);
    getActivity().unregisterReceiver(mNetworkStatus);
  }

  @Override
  public void onListItemClick(ListView liestView, View view, int position, long id) {
    super.onListItemClick(liestView, view, position, id);
    if (mListener != null) {
      mListener.onVenueSelected(position);
    }
  }

  /**
   * Show the animation load and start the service to downloaded data.
   */
  private void loadList() {
    Intent i = new Intent(getActivity(), DownloadVenueDataService.class);
    setListShown(false);
    getActivity().startService(i);
  }

  /**
   * Called when on item is selected. This method use the {@link OnVenueSelected}
   */
  private void showItems() {
    setListShown(true);
    if (DownloadVenueDataService.data != null) {
      mAdapter = new VenueListAdapter(getActivity(), DownloadVenueDataService.data);
      setListAdapter(mAdapter);
      mLoadData = false;
    }
  }

  public boolean getConnectivityStatus(Context context) {
    ConnectivityManager cm = (ConnectivityManager) context
        .getSystemService(Context.CONNECTIVITY_SERVICE);

    NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
    if (null != activeNetwork) {
      if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI)
        return true;

      if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE)
        return true;
    }
    return false;
  }

}
