package com.mad.phunware.venue.adapter;

import java.util.List;

import android.content.Context;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.mad.phunware.venue.R;
import com.mad.phunware.venue.json.ScheduleItem;

/**
 * Adapter to help draw a Schedule Item.
 * @author Mauricio Jovel.
 *
 */
public class ItemListAdapter extends ArrayAdapter<ScheduleItem> {
	// The context is called.
	private Context mContext;
	
	/**
	 * Class to recycle the View
	 * @author Mauricio Jovel.
	 *
	 */
	static class Holder {
		// TextView
		TextView text;
	}
	
	/**
	 * Default constructor.
	 * @param context Activity called.
	 * @param items Items to show.
	 */
	public ItemListAdapter(Context context, List<ScheduleItem> items) {
		super(context, android.R.layout.simple_list_item_1, items);
		this.mContext = context;
	}
	
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View rootView = convertView;
		Holder holderData = null;
		java.text.DateFormat longDateFormat = 
				DateFormat.getMediumDateFormat(mContext);
		java.text.DateFormat hourFormat = DateFormat.getTimeFormat(mContext);
		ScheduleItem sc = getItem(position);
		LayoutInflater inflater = null;
		if(rootView == null) {
			inflater = LayoutInflater.from(mContext);
			rootView = inflater.inflate(android.R.layout.simple_list_item_1, parent
					, false);
			holderData = new Holder();
			holderData.text = (TextView) rootView.findViewById(android.R.id.text1);
			rootView.setTag(holderData);
		} else {
			holderData = (Holder) rootView.getTag();
		}
		holderData.text.setText(longDateFormat.format(sc.getStartDate())
				+ " " + hourFormat.format(sc.getStartDate())
				+ " "+mContext.getResources().getString(R.string.to)
				+ " "+hourFormat.format(sc.getEndDate()));
		return rootView;
	}
	

}
