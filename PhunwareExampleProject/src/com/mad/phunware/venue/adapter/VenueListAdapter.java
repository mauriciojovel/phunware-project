package com.mad.phunware.venue.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.mad.phunware.venue.R;
import com.mad.phunware.venue.json.Venue;

/**
 * Adapter help draw a Venue item.
 * @author Mauricio Jovel
 *
 */
public class VenueListAdapter extends ArrayAdapter<Venue> {
	
	// Context Activity called.
	private Context mContext;
	
	/**
	 * Default Constructor.
	 * @param context Activity called.
	 * @param data Items to show.
	 */
	public VenueListAdapter(Context context, List<Venue> data) {
		super(context, R.layout.venue_row, data);
		this.mContext = context;
	}
	
	/**
	 * Class to recycle the View
	 * http://developer.android.com/training/articles/perf-tips.html
	 * http://java.dzone.com/articles/optimizing-your-listview
	 * @author Mauricio Jovel.
	 *
	 */
	static class Holder {
		// Title of row.
		TextView title;
		// Subtitle of row.
		TextView subTitle;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View rootView = convertView;
		Holder holderData = null;
		Venue actualVenue = getItem(position);
		LayoutInflater inflater = null;
		if(rootView == null) {
			inflater = LayoutInflater.from(mContext);
			rootView = inflater.inflate(R.layout.venue_row, parent, false);
			holderData = new Holder();
			holderData.title = (TextView) rootView.findViewById(android.R.id.text1);
			holderData.subTitle = (TextView) rootView.findViewById(android.R.id.text2);
			rootView.setTag(holderData);
		} else {
			holderData = (Holder) rootView.getTag();
		}
		
		holderData.title.setText(actualVenue.getName());
		holderData.subTitle.setText(actualVenue.getAddress()
				+ ", " + actualVenue.getCity()
				+ ", " + actualVenue.getState());
		
		return rootView;
	}

}
