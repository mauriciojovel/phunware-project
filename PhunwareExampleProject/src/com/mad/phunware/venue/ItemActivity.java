package com.mad.phunware.venue;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;

import com.mad.phunware.venue.R;
import com.mad.phunware.venue.fragments.VenueItemFragment;

/**
 * Activity represents a selected venue.
 * 
 * @author Mauricio Jovel.
 * 
 */
public class ItemActivity extends ActionBarActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_item);

    if (savedInstanceState == null) {
      VenueItemFragment venuItemFragment = new VenueItemFragment();
      venuItemFragment.setArguments(this.getIntent().getExtras());
      getSupportFragmentManager().beginTransaction().add(R.id.container
          , venuItemFragment).commit();
    }

    setUpActionBar();
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    int id = item.getItemId();
    if (id == android.R.id.home) {
      finish();
      return true;
    }
    return super.onOptionsItemSelected(item);
  }

  /**
   * Set up the supported action bar.
   */
  private void setUpActionBar() {
    getSupportActionBar().setHomeButtonEnabled(true);
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
  }

}
